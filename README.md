# Application Blueprint for Memos

## What is Memos?
Memos is an open-source, self-hosted memo hub with knowledge management and socialization.

**Documentation at:** [https://usememos.com](https://usememos.com)


This Application Blueprint is made possible by the [Community Maintained One Click Apps repository](https://github.com/caprover/one-click-apps).
If there are any apps you would like to help find their place in our Free and Open Cloud, consider contributing to One-Click Apps as well.
For more information on CapRover or their one One-Click Apps platform, visit [caprover.com](https://caprover.com/docs/one-click-apps.html).

`ensemble-generated.yaml` was generated from [https://github.com/caprover/one-click-apps/blob/master/public/v4/apps/memos.yml](https://github.com/caprover/one-click-apps/blob/master/public/v4/apps/memos.yml).  Changes can instead be applied to `ensemble-template.yaml` to overwrite the generated values.
